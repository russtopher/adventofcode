use std::collections::HashSet;

use nom::bytes::complete::{is_not, tag};
use nom::character::complete::newline;
use nom::IResult;
use nom::multi::separated_list;
use nom::sequence::separated_pair;

use crate::parse::parse_i32;

pub fn main() {
    let input = include_str!("input");

    let input = parse_input(&input);

    let part1 = solve(&input);

    println!("The solution to part 1 is {}", part1);

    let part2 = solve2(&input);
    println!("The solution to part 2 is {}", part2);
}

#[derive(Debug, Clone)]
struct Input {
    initial_state: HashSet<(i32, i32, i32)>,
}

fn parse_input(input: &str) -> Input {
    let parts: Vec<&str> = input.lines()
        .map(|line| parse_line(line))
        .

    Input {

    }
}

fn solve(input: &Input) -> usize {
    todo!()
}

fn solve2(input: &Input) -> usize {
    todo!()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_example() {
        let input = include_str!("example");
        let input = parse_input(input);

        let expected = 71;
        let actual = solve(&input);

        assert_eq!(actual, expected)
    }

    #[test]
    fn test_solution() {
        let input = include_str!("input");
        let input = parse_input(input);

        let expected = 25972;
        let actual = solve(&input);

        assert_eq!(actual, expected)
    }

    #[test]
    fn test_compute_labels() {
        let input = include_str!("example");
        let input = parse_input(input);

        let expected = vec!["row", "class", "seat"];
        let actual = compute_labels(&input);

        assert_eq!(actual, expected)
    }

    #[test]
    fn test_solution2() {
        let input = include_str!("input");
        let input = parse_input(input);

        let expected = 622670335901;
        let actual = solve2(&input);

        assert_eq!(actual, expected)
    }
}
